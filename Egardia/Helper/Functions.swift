//
//  Functions.swift
//  Egardia
//
//  Created by Amin Shafiee on 1/6/1398 AP.
//  Copyright © 1398 Amin Shafiee. All rights reserved.
//

import UIKit

class Functions: NSObject {
    static func Dwidth() -> CGFloat {   return UIScreen.main.bounds.size.width  }
    static func Dhight() -> CGFloat {   return UIScreen.main.bounds.size.height  }
    
    static func showAllert( title:String, body:String, _ holder:UIViewController ) {
        let alert = UIAlertController(title: title, message: body, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Back", style: .default, handler: nil))
        holder.present(alert, animated: true)
    }
}
