//
//  CarImageCell.swift
//  Egardia
//
//  Created by Amin Shafiee on 1/6/1398 AP.
//  Copyright © 1398 Amin Shafiee. All rights reserved.
//

import UIKit

class CarImageCell: UITableViewCell {

    @IBOutlet weak var carImageIV: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
