//
//  CarDetailsViewController.swift
//  Egardia
//
//  Created by Amin Shafiee on 1/6/1398 AP.
//  Copyright © 1398 Amin Shafiee. All rights reserved.
//

import UIKit

class CarDetailsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private let reuseIdentifierImageCell = "CarImageCellId"
    private let reuseIdentifierInfoCell = "CarInformationCellId"
    
    var selectedCar: CarModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Car Details"
        setuptableView()
    }
    
    func setuptableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        tableView.register(UINib(nibName: "CarImageCell", bundle: nil), forCellReuseIdentifier: reuseIdentifierImageCell)
        tableView.register(UINib(nibName: "CarInformationCell", bundle: nil), forCellReuseIdentifier: reuseIdentifierInfoCell)
    }

}


extension CarDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 150 : UITableView.automaticDimension
    }
}


extension CarDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
             let cell = tableView.dequeueReusableCell(withIdentifier:reuseIdentifierImageCell ) as! CarImageCell
            cell.carImageIV.loadImage(fromURL: selectedCar.picture)
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier:reuseIdentifierInfoCell ) as! CarInformationCell
            switch indexPath.row {
            case 1:
                cell.rowTitleLbl.text = "Make:"
                cell.rowvalueLbl.text = selectedCar.make
                break
            case 2:
                cell.rowTitleLbl.text = "Model:"
                cell.rowvalueLbl.text = selectedCar.model
                break
            case 3:
                cell.rowTitleLbl.text = "Price:"
                cell.rowvalueLbl.text = selectedCar.price
                break
            case 4:
                cell.rowTitleLbl.text = "Year:"
                cell.rowvalueLbl.text = selectedCar.year
                break
            default :
                cell.rowTitleLbl.text = "KiloMeter:"
                cell.rowvalueLbl.text = selectedCar.km
            }
            return cell
        }
    }
}

