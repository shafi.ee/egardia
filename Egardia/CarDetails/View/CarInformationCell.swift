//
//  CarInformation.swift
//  Egardia
//
//  Created by Amin Shafiee on 1/6/1398 AP.
//  Copyright © 1398 Amin Shafiee. All rights reserved.
//

import UIKit

class CarInformationCell: UITableViewCell {

    @IBOutlet weak var rowTitleLbl: UILabel!
    @IBOutlet weak var rowvalueLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
