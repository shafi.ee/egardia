//
//  CarsListViewModel.swift
//  Egardia
//
//  Created by Amin Shafiee on 1/6/1398 AP.
//  Copyright © 1398 Amin Shafiee. All rights reserved.
//

import UIKit

class CarsListViewModel: NSObject {

    public var cars : (([CarModel])->Void)?
    public var onError : ((String)->Void)?
    
    func fetchCars(){
        let url = URL(string: "https://raw.githubusercontent.com/beckershoff/Egardia-Mobile-Development-Assessment/master/cars.json")
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            
            guard error == nil else {
                self.onError?("Ops! Something was wrong on server or network connection!")
                return
            }
            guard let content = data else {
                self.onError?("The data return by server was null")
                return
            }
            
            do{
                let jsonResponse = try JSONDecoder().decode([CarModel].self, from: content)
                DispatchQueue.main.async {
                    self.cars?(jsonResponse)
                }
            } catch let parsingError {
                self.onError?("Ops! Something was wrong. \(parsingError.localizedDescription)")
            }
        }
        task.resume()
    }
}
