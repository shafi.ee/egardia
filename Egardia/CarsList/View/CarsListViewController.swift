//
//  ViewController.swift
//  Egardia
//
//  Created by Amin Shafiee on 1/6/1398 AP.
//  Copyright © 1398 Amin Shafiee. All rights reserved.
//

import UIKit

class CarsListViewController: UIViewController {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    var navigationBar: UINavigationBar?
    
    var viewModel = CarsListViewModel()
    var cars: [CarModel] = [CarModel]() {
        didSet {
            self.indicator.stopAnimating()
            self.collectionView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
        navigationBar = self.navigationController?.navigationBar
        self.view.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
        
        navigationBar?.tintColor = .white
        navigationBar?.backgroundColor = UIColor("fc6621")
        navigationBar?.barTintColor = UIColor("fc6621")
        navigationBar?.isTranslucent = false
        navigationBar?.titleTextAttributes =  [.foregroundColor:UIColor.white ]
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Cars List"
        indicator.startAnimating()
        setupCollectionView()
        
        viewModel.fetchCars()
        viewModel.cars = { [weak self] (serverResponseCars) in
            guard let `self` = self else {return}
            self.cars = serverResponseCars
        }
        viewModel.onError = { error in
            Functions.showAllert(title: "Error", body: error, self)
        }
        
    }
    
    func setupCollectionView(){
        collectionView.register(UINib(nibName: "CarsListCell", bundle: nil), forCellWithReuseIdentifier: "CarsListCellId")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
    }
    
    func showCarDetails(_ car: CarModel ){
        let page = CarDetailsViewController()
        page.selectedCar = car
        self.navigationController?.pushViewController(page, animated: true)
    }
    
}


extension CarsListViewController: UICollectionViewDataSource  {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cars.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarsListCellId", for: indexPath) as! CarsListCell
        cell.car = cars[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let car = cars[indexPath.row]
        showCarDetails(car)
    }
}

extension CarsListViewController : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: Functions.Dwidth()/2-20, height: 100)
        } else {
            return CGSize(width: Functions.Dwidth()-20, height: 100)
        }
    }
}
