//
//  CarsListCell.swift
//  Egardia
//
//  Created by Amin Shafiee on 1/6/1398 AP.
//  Copyright © 1398 Amin Shafiee. All rights reserved.
//

import UIKit

class CarsListCell: UICollectionViewCell {

    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var carModelLbl: UILabel!
    @IBOutlet weak var carMakeLbl: UILabel!
    @IBOutlet weak var carPriceLbl: UILabel!
    
    var car : CarModel! {
        didSet {
            self.carImageView.loadImage(fromURL: car.picture)
            self.carModelLbl.text = car.model
            self.carMakeLbl.text = car.make
            self.carPriceLbl.text = "\(car.price) $"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor("c2c2c2")
        self.carImageView.layer.cornerRadius = 3
        self.carImageView.clipsToBounds = true
    }

}
