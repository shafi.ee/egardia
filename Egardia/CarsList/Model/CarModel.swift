//
//  CarModel.swift
//  Egardia
//
//  Created by Amin Shafiee on 1/6/1398 AP.
//  Copyright © 1398 Amin Shafiee. All rights reserved.
//

import UIKit

struct CarModel: Codable {
    let make, model, price, year, km, picture: String
}


// MARK: Convenience initializers
extension CarModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(CarModel.self, from: data) else { return nil }
        self = me
    }
}
